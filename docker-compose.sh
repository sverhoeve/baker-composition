#!/bin/bash

# Navigate to script directory
cd "$(dirname "$0")"

mkcert -key-file ./docker/certs/ssl.key -cert-file ./docker/certs/ssl.crt '*.toptaarten.dev'

export CAROOT=$(mkcert -CAROOT)
docker-compose stop && docker-compose up -d

docker exec -it baker-system_php-fpm bash
