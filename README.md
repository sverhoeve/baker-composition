###

#### Installeer `mkcert` op OSX;
- https://formulae.brew.sh/formula/mkcert

```shell
mkcert -key-file ssl.key -cert-file ssl.crt '*.toptaarten.dev'
```

#### Certificaat aanmaken voor test domein:
```shell
mkcert -install # Install the local CA in the system trust store.
mkcert example.org # Generate "example.org.pem" and "example.org-key.pem".
mkcert example.com myapp.dev localhost 127.0.0.1 ::1 # Generate "example.com+4.pem" and "example.com+4-key.pem".
mkcert "*.example.it" # Generate "_wildcard.example.it.pem" and "_wildcard.example.it-key.pem".
mkcert -uninstall # Uninstall the local CA (but do not delete it).
```

#### Build maken voor productie (met locale ssh keys die gebruikt kunnen worden voor fetchen voor afhankelijkheden):
```shell
ssh-add \
&& DOCKER_BUILDKIT=1 docker build --ssh default --target production -t baker_system .
```

#### Build maken voor development (met locale ssh keys die gebruikt kunnen worden voor fetchen voor afhankelijkheden):
```shell
ssh-add \
&& DOCKER_BUILDKIT=1 docker build --ssh default --target development -t baker_system-dev .
```

#### Run docker image
```shell
docker run -p 8080:9000 -it baker_system 
```
