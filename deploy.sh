#!/bin/bash

# Navigate to script directory
cd "$(dirname "$0")"

SSH_KEY=$(base64 -i ~/.ssh/id_rsa)
export GOOGLE_APPLICATION_CREDENTIALS=./gcp.json
export GOOGLE_OAUTH_ACCESS_TOKEN=$(gcloud auth application-default print-access-token)
docker login -u oauth2accesstoken --password ${GOOGLE_OAUTH_ACCESS_TOKEN} https://eu.gcr.io

# Ophalen van beta release
docker pull eu.gcr.io/management-22bfd4e4/baker-system:beta

kubectl apply -k kustomization
